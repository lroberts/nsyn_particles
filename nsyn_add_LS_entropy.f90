program add_LS_entropy
  use LS_eos_wrapper_mod
  implicit none

  character(80) :: infile,ofile
  integer :: i,cstat

  do i=0,99
    write(infile,'(a,i0.3)')'./particles_with_expansion/particle',i
    write(ofile, '(a,i0.3)')'./particles_with_Helmholtz_temp/particle',i
    call EXECUTE_COMMAND_LINE('test -e '//TRIM(infile),EXITSTAT=cstat)

    if (cstat.ne.0) then
      write(6,'(3a)')'The File ',TRIM(infile),' does not exist.'
      cycle
    endif
    call rewrite_particle(infile,ofile)
    write(6,'(3a)')'File ',TRIM(infile),' done'
  enddo

contains

  subroutine rewrite_particle(fin,fout)
    character(*), intent(in) :: fin,fout
    integer :: iunit,ounit,np,ncomment,istat,ierr
    real(8) :: ye
    character(1000) :: line,fline
    real(8) :: tt,T9,rho,ee,rad,TMeV,nb,ss
    real(8) :: eeos,peos,seos,feos
    real(8) :: T,abar,zbar, X_nut, X_prot, X_alpha, X_heavy, A_heavy, pp,eint, Helm_p, Helm_s, Helm_eint
    real(8) :: T_out, T_helm_last
    logical :: helmholtz_success
    iunit = 12
    ounit = 13

    ! Determine how many data points there are
    open(iunit,file=TRIM(fin ),STATUS='old',ACTION='read')
    open(ounit,file=TRIM(fout))
    ss = -1.d0
    np = 0 ! Start at minus one because of the Ye line
    T = -1.d0
    T_helm_last = 1.0d99
    do while (.true.)

      read(iunit,'(a)',IOSTAT=istat) fline
      if (istat.ne.0) exit ! End of the file

      ! Check if this is a comment line
      ncomment = INDEX(fline,'#')
      if (ncomment<1) ncomment = LEN(line)+1
      line = fline(1:ncomment-1)
      if (LEN_TRIM(line)>0) then
        if (np==0) then
          read(line,*)ye
          write(ounit,'(es20.10)')ye
        else
          read(line,*)tt,T9,rho,ee,rad
          TMeV = T9 / 11.604505289680862d0
          nb = rho * 6.02214129d-16 ! baryon mass, 1/N_A
          ! this gives worse agreement with LS220
          !nb = rho * 5.9704087045248921d-16 ! neutron mass
          if (np==1) write(ounit,'(a)') '# [6] entropy [kb baryon^-1]'
          if (np==1) write(ounit,'(a)') '# [7] Helmholtz temperature [GK]'
          ierr = LS_eos_wrapper(nb,TMeV,ye,eeos,peos,seos,feos,abar, X_nut, X_prot, X_alpha, X_heavy, A_heavy)

          if (ierr == 0) then
            ss = seos
          endif

          if (T<0.d0) T = 1.d9*T9
          zbar = abar*ye

          !call Helm_eos_wrap(rho, 1.d9*T9, Helm_p, Helm_s, Helm_eint, abar, zbar, X_nut, X_prot, X_alpha, X_heavy, A_heavy)

          if (ss > 0.0) then
            if (T_helm_last > 1.0d3) then
              call Helm_eos_wrap_S(rho,T,pp,ss,eint,abar,zbar, X_nut, X_prot, X_alpha, X_heavy, &
              A_heavy, helmholtz_success)
            else
              T = T_helm_last
            endif

            if (helmholtz_success) then
              T_out = T
            else
              T_out = 1.0d3
            endif
            
            T_helm_last = T_out
          else
            T = -1.d0
            T_out = 1.0d99
            if (np <= 1) then
              write(6,'(a,a,a)')'The file ',TRIM(fin),' has negative entropy.'
            endif
          endif
          write(ounit,'(20es20.10)')tt,T9,rho,ee,rad,ss,T_out*1.d-9
        endif
        np = np + 1
      else
        write(ounit,'(a)')TRIM(fline)
      endif

    enddo

    if (ss<0.d0) then
      write(6,'(a,a,a)')'The file ',TRIM(fin),' is never on the EoS table.'
    endif

    close(iunit)
    close(ounit)
  end subroutine rewrite_particle
end program add_LS_entropy
