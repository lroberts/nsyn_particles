program compare_helmholtz_LS
  use LS_eos_wrapper_mod
  implicit none 

  include 'const.dek'
  
  real(8) :: rho_min, rho_max     ! in fm^{-3}
  real(8) :: temp_min, temp_max   ! in GK
  real(8) :: ye_min, ye_max
  integer :: num_rho, num_temp, num_ye

  character(80) :: ofile
  integer :: ounit, r, t, y

  real(8) :: rho, rho0, drho, temp, temp0, dtemp, ye, ye0, dye
  real(8) :: entropy_LS, entropy_Helmholtz

  real(8) :: Abar, X_nut, X_prot, X_alpha, X_heavy, A_heavy
  
  ! for LS
  real(8) :: TMeV, eeos, peos, seos, feos
  integer :: ierr

  ! for Helmholtz
  real(8) :: rho_g_per_cm3, P,  ee, x, s, z, sioncon
  real(8), parameter :: Na = 6.0221417930d23
  real(8), parameter :: kb = 1.380650424d-16
  
  ! set limits
  rho_min = 1.0d-7
  rho_max = 1.0d-5
  temp_min = 5.0d-1
  temp_max = 2.0d0
  ye_min = 1.0d-2
  ye_max = 4.0d-1

  ! set number of points
  num_rho = 1
  num_temp = 1
  num_ye = 1

  rho_min = 4.7486978940E-06
  temp_min = 6.7827288795E-01
  ye_min = 4.0019693599E-02

  ! set output file
  write(ofile, '(a)') 'compare_helmholtz_LS.out'
  ounit = 13
  open(ounit,file=TRIM(ofile))

  rho0 = log(rho_min)
  if (num_rho > 1) then
    drho = (log(rho_max) - rho0) / real(num_rho - 1)
  else
    drho = 0.0d0
  endif
  
  temp0 = log(temp_min)
  if (num_temp > 1) then
    dtemp = (log(temp_max) - temp0) / real(num_temp - 1)
  else
    dtemp = 0.0d0
  endif

  ye0 = log(ye_min)
  if (num_ye > 1) then
    dye = (log(ye_max) - ye0) / real(num_ye - 1)
  else
    dye = 0.0d0
  endif
  
  do t = 1, num_temp
    temp = exp(temp0 + real(t-1) * dtemp)
    TMeV = temp * 0.08617342790900662

    do r = 1, num_rho
      rho = exp(rho0 + real(r-1) * drho)

      do y = 1, num_ye
        ye = exp(ye0 + real(y-1) * dye)

        ierr = LS_eos_wrapper(rho, TMeV, ye, eeos, peos, entropy_LS, feos, Abar, X_nut, X_prot, X_alpha, X_heavy, A_heavy)
!         if (ierr == 0) then
!           entropy_LS = seos
!         else
!           entropy_LS = 0.0d0
!         endif

        rho_g_per_cm3 = rho / 5.9704087045248921E-16
        call Helm_eos_wrap(rho_g_per_cm3, temp * 1.0d9, P, entropy_Helmholtz, ee, Abar, Abar * ye, X_nut, X_prot, &
                           X_alpha, X_heavy, A_heavy)

        ! calculate neutron 1/(n\Lambda^3)
        x       = 1.0d0 / (rho_g_per_cm3 * avo * X_nut)
        sioncon = (2.0d0 * pi * amu * kerg)/(h*h)
        s       = sioncon * temp * 1.0d9 * 1.0d0
        z       = x * s * sqrt(s)

        !write(ounit,'(20es20.10)') temp, rho, rho_g_per_cm3, ye, z, X_nut, X_prot, X_alpha, X_heavy, entropy_LS, entropy_Helmholtz
        write(ounit,'(20es20.10)') rho, TMeV, ye, entropy_LS
      enddo
    enddo
  enddo
  
  close(ounit)
  
end program compare_helmholtz_LS