#--------------------------------------------------------------------------
#
#         makefile for PNTTST (you must edit the compilation macros
#                              section to use this file)
#
#         usage:  "make"  builds PNTTST
#                 "make tar" builds tar archive of source & initial
#                            data files.
#                 "make clean" deletes the "*.o" & "pnttst" files
#
#--------------------------------------------------------------------------
#                 List of object modules
#--------------------------------------------------------------------------
A = pnttst.o
B = lseos_v2.7.o
#
#--------------------------------------------------------------------------
#                 Compilation macros
#            Uncomment the set that you need. Some platforms have more
#            than one definition of "FFLAGS" or "LFLAGS" defined.
#            This usually means that one set has optimization turned
#            on while the other has the debugging & error checking
#            turned on.
#                 
#--------------------------------------------------------------------------
#
#
#                     Generic
FORT = gfortran
FFLAGS = -c
LINK = gfortran
LFLAGS = -o
#
#                     Silicon Graphics (IRIX 5.2)
#FORT = f77
#FFLAGS = -w1 -c
#FFLAGS = -w1 -g -trapuv -C -lfpe -c
#LINK = f77
#LFLAGS = -o
#
#
#                     CRAY (2, X-MP, Y-MP, C-90)
#FORT = cf77
#FFLAGS = -Wf"-dp -eiz -o off" -c
#FFLAGS = -Wf"-dp" -c
#LINK = segldr
#LFLAGS = -f indef -o
#
#                     DECstation (3100, 5000, & AXP)
#FORT = f77
#FFLAGS = -w1 -g -fpe -C -c
#LINK = f77
#LFLAGS = -o
#
#                     MasPar (MP-1)
#FORT = mpfortran
#FFLAGS = -c -fecommon -hprofile -report -pesize=16 -Omax
#LINK = mpfortran
#LFLAGS = -pesize=16 -report -hprofile -Omax -o
#
#                     Sun Sparc (IPC)
#FORT = f77
#FFLAGS = -c -w -sb -g
#LINK = f77
#LFLAGS = -sb -g -o
#
#                     IBM (RS/6000)
#                                  Add -qsource to get source listing
#FORT = xlf
#FFLAGS = -c -g -qflttrap=overflow:zerodivide:invalid:enable
#FFLAGS = -c -O
#LINK = xlf
#LFLAGS =  -g -qflttrap=overflow:zerodivide:invalid:enable -o
#LFLAGS =  -O -o
#
#                     Convex (C3880)
#FORT = fc
#FFLAGS = -c -cxdb -g -no
#LINK = fc
#LFLAGS = -cxdb -g -no -o

#
#--------------------------------------------------------------------------
nsyn_add_LS_entropy: $(B) LS_eos_wrapper.o helmholtz_eos.o nsyn_add_LS_entropy.o 
	$(LINK) $(LFLAGS) nsyn_add_LS_entropy $(B) helmholtz_eos.o LS_eos_wrapper.o nsyn_add_LS_entropy.o
#compare_helmholtz_LS: $(B) LS_eos_wrapper.o helmholtz_eos.o compare_helmholtz_LS.o
#	$(LINK) $(LFLAGS) compare_helmholtz_LS $(B) helmholtz_eos.o LS_eos_wrapper.o compare_helmholtz_LS.o

#
#--------------------------------------------------------------------------
pnttst : $(A) $(B)
	$(LINK) $(LFLAGS) pnttst $(A) $(B)

nsyn_add_LS_entropy.o: nsyn_add_LS_entropy.f90
	$(FORT) $(FFLAGS) nsyn_add_LS_entropy.f90
#compare_helmholtz_LS.o: compare_helmholtz_LS.f90
#	$(FORT) $(FFLAGS) compare_helmholtz_LS.f90
LS_eos_wrapper.o: LS_eos_wrapper.f
	$(FORT) $(FFLAGS) LS_eos_wrapper.f
helmholtz_eos.o: helmholtz_eos.f90
	$(FORT) $(FFLAGS) helmholtz_eos.f90

#--------------------------------------------------------------------------
#
#          Main program
pnttst.o : pnttst.f eos_m4c.inc maxwel.inc el_eos.inc  force.inc
	$(FORT) $(FFLAGS) pnttst.f
#
#
#          EOS subroutine
lseos_v2.7.o : lseos_v2.7.f eos_m4c.inc maxwel.inc el_eos.inc  force.inc
	$(FORT) $(FFLAGS) lseos_v2.7.f
#
#--------------------------------------------------------------------------
#                 Make a tar file of all this
#--------------------------------------------------------------------------
#
tar: 
	tar -cvf lseos_v2.7.tar makefile pnttst.f lseos_v2.7.f \
eos_m4c.inc maxwel.inc el_eos.inc force.inc \
bd180.atb max180.atb \
bd220.atb max220.atb \
bd375.atb max375.atb \
notes.ps notes.tex README CHANGES
#
#
#--------------------------------------------------------------------------
#                 Clean up the *.o files & the executable
#--------------------------------------------------------------------------
#
clean:
	/bin/rm *.o pnttst
#
#
#
#--------------------------------------------------------------------------






