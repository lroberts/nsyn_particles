      module LS_eos_wrapper_mod
      contains
      function LS_eos_wrapper(nbin,Tin,yein,pp,uu,ss,ff,out_abar,
     1 X_nut, X_prot, X_alpha, X_heavy, A_heavy) result(ierr)
C
      IMPLICIT NONE
C
      integer :: ierr
      real(8), intent(in)  :: nbin,Tin,yein
      real(8), intent(out) :: pp,uu,ss,ff,out_abar
      real(8), intent(out) :: X_nut,X_prot,X_alpha,X_heavy,A_heavy

      logical, save :: initialized = .false.

      INTEGER IFLG, EFLG, FFLG, SF
      DOUBLE PRECISION IPVAR(4), TOLD, INPYE, RHO, XPR, PPR, INPT
      real(8) :: sumX, sumXdivA
C
C                   The following include files are only needed if you
C                   want to examine the EOS outputs.  Otherwise they
C                   can remain commented out.
      INCLUDE 'eos_m4c.inc'
C      INCLUDE 'el_eos.inc'
C      INCLUDE 'maxwel.inc'
C
C----------------------------------------------------------------------
C----------------------------------------------------------------------
C
C                   Initialize the boundary and fermi integral tables
      if (.not.initialized) then
        CALL LOADMX()
        initialized = .true.
      endif
      INPT  = Tin
      RHO   = nbin
      INPYE = yein
      ierr  = 0
      ! Check that we are in the table region
      if (Tin<0.05d0)  ierr = ierr + 1
      if (nbin<1.d-8)  ierr = ierr + 2
      if (yein<0.03d0) ierr = ierr + 4
      if (yein>0.55d0) ierr = ierr + 8
      if (ierr>0) then
        return
      endif
C
C                            Now set the EOS inputs:
C
C                   Set input flag to indicate temperature as input
      IFLG = 1
C                   Set "forcing" flag to zero
      FFLG = 0
C                   Set temperature
      IPVAR(1) = INPT
C                   Set initial guess at nuclear density
      IPVAR(2) = 0.155D0
C                   Set initial guess at proton eta
      IPVAR(3) = -15.d0
C                   Set initial guess at neutron eta
      IPVAR(4) = -10.0d0
C                   Set initial guess at exterior proton fraction
      PPR = RHO*INPYE
C
C
C
C                   Now call the EOS
      CALL INVEOS(IPVAR,TOLD,INPYE,RHO,IFLG,EFLG,FFLG,SF,XPR,PPR)
C
      if (SF .NE. 1) then
        ierr = 16
        return
      endif

      pp = PTOT
      uu = UTOT
      ss = STOT
      ff = FTOT

C     calculate Abar (average A) = (sum_i X_i) / (sum_i X_i/A_i)

C     sum_i X_i (should be 1 but calculate anyway)
      sumX = XNUT + XPROT + XALFA + XH
      sumXdivA = XNUT + XPROT + XALFA / 4 + XH / A
      out_abar = sumX / sumXdivA

      X_nut = XNUT
      X_prot = XPROT
      X_alpha = XALFA
      X_heavy = XH
      A_heavy = A

      end function LS_eos_wrapper
      end module LS_eos_wrapper_mod
