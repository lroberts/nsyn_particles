set term pdfcairo enhanced size 8,6
set output "ok_sph_particles.pdf"

do for [i = 1:500] {

id = sprintf("%03i", i)
file = 'particle'.id

# only plot if file exists
if (system("[ ! -f ".file." ]; echo $?")) {

system("echo '".file."'")

last_record = "<(tail -n 1 ".file.")"

stats last_record using 1 name 'time' nooutput
stats last_record using 3 name 'density' nooutput
stats last_record using 4 name 'entropy' nooutput
stats last_record using 5 name 'energy' nooutput
stats last_record using 6 name 'radius' nooutput

set logscale xy

set xtics format "10^{%T}"
set ytics format "10^{%T}"

set multiplot layout 2,2 title "GOOD Particle ".id

set xrange [1e-3:10]

set xlabel "time [s]"
set ylabel "density [g cm^{-3}]"
set key bottom left
set yrange [1e-2:1e10]
plot density_max * (x / time_max)**(-3.0) w l t "t^{-3}" lc 0, \
file u 1:3 w l t "density" lc 1


set xlabel "time [s]"
set ylabel "radius [code units]"
set key top left
set yrange [1e0:1e6]
plot radius_max * (x / time_max)**(1.0) w l t "t^{1}" lc 0, \
file u 1:6 w l t "radius" lc 1, \
'' u 1:(abs($9)) w l t "|z|" lc 3


unset logscale y
set xlabel "time [s]"
set ylabel "specific energy [cm^2 s^{-2}]"
set y2label "entropy [kB / baryonn]"
set key bottom right
set ytics format "%.0te%T" nomirror
set y2tics
set autoscale y
set autoscale y2
set xzeroaxis lw 2 lc 0 lt 1
set label right sprintf("final value = %.2E", energy_max) at graph 0.95, graph 0.2
plot file u 1:5 w l t "specific energy", '' u 1:4 w l lc 3 t "entropy" axes x1y2
unset label
unset y2label
unset y2tics
set ytics mirror
set logscale y

unset logscale xy
set xlabel "x [code units]"
set ylabel "y [code units]"
set xtics format "%.0te%T"
set ytics format "%.0te%T"
set autoscale xy
set xzeroaxis lw 2 lc 0 lt 1
set yzeroaxis lw 2 lc 0 lt 1
set label center "trajectory" at graph 0.5, graph 0.9
plot file u 7:8 w l notitle
unset label
set logscale xy

unset multiplot

unset logscale xy

}
}

unset output
