#!/usr/bin/env python

root = '../particles_with_Helmholtz_temp/'

import sys
import os.path
import numpy
from cStringIO import StringIO

for i in range(100):
    sys.stdout.write('particle %03d: ' % i)

    path = root + 'particle%03d' % i;
    if (not os.path.isfile(path)):
        sys.stdout.write('does not exist\n')
        continue

    # read file
    f = open(path, 'r')
    str_data = StringIO()
    ye = 0.0

    for line in f:
        line = line.strip()

        # ignore comments
        if (line[0] == '#'):
            continue

        # get Ye value
        if (len(line.split()) == 1):
            ye = float(line.split()[0])
            continue

        str_data.write(line + '\n')

    str_data.seek(0)
    dat = numpy.loadtxt(str_data)
    f.close()

    if ((ye < 0.03) or (ye > 0.55)):
        sys.stdout.write('invalid Ye = %f\n' % ye)
        continue

    entropy = dat[:,5]
    density = dat[:,2] * 6.02214129E-16
    LS220_temp = dat[:,1]
    Helmholtz_temp = dat[:,6]

    if (any(entropy < 0.0)):
      sys.stdout.write('%i INVALID ENTROPIES\n' % sum(entropy < 0.0))
      continue

    first_frac = 100*(Helmholtz_temp[0] - LS220_temp[0]) / LS220_temp[0]

    idx = density >= 1E-7
    if (all(numpy.logical_not(idx))):
        sys.stdout.write('first = %f%%\n' % first_frac)
        continue

    frac = 100*(Helmholtz_temp[idx] - LS220_temp[idx]) / LS220_temp[idx]

    if (any(Helmholtz_temp[idx] < 0.0)):
        sys.stdout.write('INVALID HELMHOLTZ TEMPERATURES ')

    sys.stdout.write('first = %f%%, min = %f%%, mean = %f%%, max = %f%%\n' % (first_frac, min(abs(frac)), sum(abs(frac))/len(frac), max(abs(frac))))
