#!/usr/bin/env python

root = '../sph_particle_data/'

good_dir = '../sph_ok_particles/'
bad_dir = '../sph_bad_particles/'

import sys
import os.path
import numpy as np
from scipy.optimize import curve_fit
from cStringIO import StringIO
import shutil

def f_line(x, a, b):
  return a*x + b

for i in range(500):
  sys.stdout.write('particle %03d: ' % i)

  path = root + 'particle%03d' % i
  if (not os.path.isfile(path)):
    sys.stdout.write('does not exist\n')
    continue

  # read file
  f = open(path, 'r')
  str_data = StringIO()
  ye = 0.0

  for line in f:
    line = line.strip()

    # ignore comments
    if (line[0] == '#'):
      if (line.startswith('# Ye is constant, Ye =')):
        ye = float(line.split()[6])

      continue

    str_data.write(line + '\n')

  str_data.seek(0)
  dat = np.loadtxt(str_data)
  f.close()

  if (dat.shape[1] != 9):
    sys.stdout.write('got %i columns instead of 8\n' % dat.shape[1])
    continue

  if ((ye < 0.03) or (ye > 0.55)):
    sys.stdout.write('invalid Ye = %f\n' % ye)
    continue

  time = dat[:,0]
  rho = dat[:,2]
  r = dat[:,5]

  log_time = np.log(time)
  log_rho = np.log(rho)
  log_r = np.log(r)

  # we match the line log_rho = -3 * log_time + B_rho with the last point, hence
  # B_rho = log_rho + 3 * log_time
  B_rho = log_rho[-1] + 3.0 * log_time[-1]
  rho_line = -3.0 * log_time + B_rho
  rho_delta2 = (rho_line - log_rho) * (rho_line - log_rho)
  # reverse the delta array (so that the last point (where delta is 0) is at the beginning)
  rho_delta2 = rho_delta2[::-1]
  rho_delta2_cumsum = np.cumsum(rho_delta2)
  # the index of the point where the cumulative sum of delta^2 first exceeds 1E-6
  rho_idx = np.argmin(rho_delta2_cumsum < 1.0E-6)

  # we match the line log_r = log_time + B_r with the last point, hence
  # B_r = log_r + log_time
  B_r = log_r[-1] - log_time[-1]
  r_line = log_time + B_r
  r_delta2 = (r_line - log_r) * (r_line - log_r)
  # reverse the delta array (so that the last point (where delta is 0) is at the beginning)
  r_delta2 = r_delta2[::-1]
  r_delta2_cumsum = np.cumsum(r_delta2)
  # the index of the point where the cumulative sum of delta^2 first exceeds 1E-6
  r_idx = np.argmin(r_delta2_cumsum < 1.0E-6)

  # do fit
  num_fit = 8000
  rho_popt, rho_pcov = curve_fit(f_line, log_time[-num_fit:], log_rho[-num_fit:])
  r_popt, r_pcov = curve_fit(f_line, log_time[-num_fit:], log_r[-num_fit:])

  c1 = False
  c2 = False

  # include if rho_idx >= 100 and r_idx >= 100
  if ((rho_idx >= 100) and (r_idx >= 100)):
    sys.stdout.write(' in ')
    c1 = True
  else:
    sys.stdout.write('out ')
    c1 = False

  if ((-3.03 <= rho_popt[0]) and (rho_popt[0] <= -2.97) and (0.99 <= r_popt[0]) and (r_popt[0] <= 1.01)):
    sys.stdout.write(' in ')
    c2 = True
  else:
    sys.stdout.write('out ')
    c2 = False

  if (c1 != c2):
    sys.stdout.write(' DISCREPANCY ')

  sys.stdout.write('rho_idx = %i, r_idx = %i ' % (rho_idx, r_idx))
  sys.stdout.write('rho: %f +/- %f, r: %f +/- %f ' % (rho_popt[0], np.sqrt(rho_pcov[0,0]), r_popt[0], np.sqrt(r_pcov[0,0])))

  if (c1 and c2):
    shutil.copy(path, good_dir + 'particle%03d' % i)
  else:
    shutil.copy(path, bad_dir + 'particle%03d' % i)

  #sys.stdout.write('max(log(rho)) = %f' % np.max(log_rho))

  sys.stdout.write('\n')
