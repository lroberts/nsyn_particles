#!/usr/bin/env python

in_root = '../particles_with_Helmholtz_temp/'
out_root = '../xnet_histories_starting_at_5GK/'

import sys
import os.path
import numpy
from cStringIO import StringIO

def format_float(x):
  return '% 25.15E' % x

for i in range(100):
  sys.stdout.write('particle %03d: ' % i)

  in_path = in_root + 'particle%03d' % i
  if (not os.path.isfile(in_path)):
    sys.stdout.write('does not exist\n')
    continue

  # read file
  f = open(in_path, 'r')
  str_data = StringIO()
  ye = 0.0

  for line in f:
    line = line.strip()

    # ignore comments
    if (line[0] == '#'):
      continue

    # get Ye value
    if (len(line.split()) == 1):
      ye = float(line.split()[0])
      continue

    str_data.write(line + '\n')

  str_data.seek(0)
  dat = numpy.loadtxt(str_data)
  f.close()

  time = dat[:,0]
  density = dat[:,2]
  temp = dat[:,6]

  idxs = numpy.arange(len(time))

  start_temp = 5 # start temperature in GK
  # find first point where temperature is below start_temp
  # instead of taking the first point or which the temperature is
  # less than start_temp, we use the point after the last point for which it is
  # greater or equal to start_temp (because we want all temperatures after the
  # beginning to be less than start_temp
  too_large_temp = idxs[temp >= start_temp]
  first_idx = 0
  if (len(too_large_temp) > 0):
    first_idx = too_large_temp[-1] + 1

  # we have only invalid temperatures
  if (first_idx >= len(time)):
    sys.stdout.write('has no valid temperatures\n')
    continue

  t_begin = time[first_idx]
  t_final = time[-1]

  # write output file
  out_path = out_root + 'particle%03d.therm' % i
  fout = open(out_path, 'w')

  fout.write(format_float(ye) + '\n')
  fout.write(format_float(t_begin) + '\n')
  fout.write(format_float(t_final) + '\n')

  for d in range(len(time)):
    fout.write(format_float(time[d]))
    fout.write(format_float(temp[d]))
    fout.write(format_float(density[d]))
    fout.write('\n')

  sys.stdout.write('done\n')

  fout.close()

