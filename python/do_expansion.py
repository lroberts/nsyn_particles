#!/usr/bin/env python

root = '../sph_ok_particles/'
out_dir = '../particles_with_expansion/particle'

import sys
import os.path
import numpy as np
from scipy.optimize import curve_fit
from cStringIO import StringIO
import shutil

def rho_line(x, b):
  return -3.0*x + b

def r_line(x, b):
  return x + b

def blend_func(x):
  return 0.5 + 15.0 * x / 16.0 - 5.0 * x**3 / 8.0 + 3.0 * x**5 / 16.0

def blend(a, b, n):
  xa = np.linspace(1.0, -1.0, n)
  xb = np.linspace(-1.0, 1.0, n)
  return a * blend_func(xa) + b * blend_func(xb)

def format_float(x):
  return '% 25.15E' % x

for i in range(100):
  sys.stdout.write('particle %03d: ' % i)

  path = root + 'particle%03d' % i
  if (not os.path.isfile(path)):
    sys.stdout.write('does not exist\n')
    continue

  # read file
  f = open(path, 'r')
  str_data = StringIO()
  ye = 0.0
  mass = 0.0

  for line in f:
    line = line.strip()

    if (line.startswith('# mass is constant, mass =')):
      mass = float(line.split()[6])
      continue

    # ignore comments
    if (line[0] == '#'):
      continue

    # get Ye value
    if (len(line.split()) == 1):
      ye = float(line.split()[0])
      continue

    str_data.write(line + '\n')

  str_data.seek(0)
  dat = np.loadtxt(str_data)
  f.close()

  if (dat.shape[1] != 5):
    sys.stdout.write('got %i columns instead of 5\n' % dat.shape[1])
    continue

  if ((ye < 0.03) or (ye > 0.55)):
    sys.stdout.write('invalid Ye = %f\n' % ye)
    continue

  if (mass <= 0.0):
    sys.stdout.write('invalid mass = %f\n' % mass)
    continue

  time = dat[:,0]
  rho = dat[:,2]
  r = dat[:,4]

  log_time = np.log(time)
  log_rho = np.log(rho)
  log_r = np.log(r)
  # do fit
  num_fit = 4000
  rho_popt, rho_pcov = curve_fit(rho_line, log_time[-num_fit:], log_rho[-num_fit:])
  r_popt, r_pcov = curve_fit(r_line, log_time[-num_fit:], log_r[-num_fit:])

  # now expand to 10^8 seconds
  num_points = 3000
  end_time = 1.0E8

  # we don't include the first point
  new_log_time = np.linspace(log_time[-1], np.log(end_time), num_points + 1)
  new_log_time = new_log_time[1:]

  # assemble pieces
  num_old = len(time)
  total_len = num_old + num_points

  new_time = np.zeros(total_len)
  new_time[0:num_old] = time
  new_time[num_old:] = np.exp(new_log_time)

  new_temp = np.zeros(total_len)
  new_temp[0:num_old] = dat[:,1]
  new_temp[num_old:] = dat[-1,1]

  new_energy = np.zeros(total_len)
  new_energy[0:num_old] = dat[:,3]
  new_energy[num_old:] = dat[-1,3]

  # for rho and r we smoothly blend the original data with the extrapolated data in the fit region
  new_rho = np.zeros(total_len)
  new_rho[0:num_old-num_fit] = rho[0:num_old-num_fit]
  new_rho[num_old-num_fit:num_old] = blend(rho[num_old-num_fit:num_old], np.exp(-3.0 * log_time[num_old-num_fit:num_old] + rho_popt[0]), num_fit)
  new_rho[num_old:] = np.exp(-3.0 * new_log_time + rho_popt[0])

  new_r = np.zeros(total_len)
  new_r[0:num_old-num_fit] = r[0:num_old-num_fit]
  new_r[num_old-num_fit:num_old] = blend(r[num_old-num_fit:num_old], np.exp(log_time[num_old-num_fit:num_old] + r_popt[0]), num_fit)
  new_r[num_old:] = np.exp(new_log_time + r_popt[0])

  fout = open(out_dir + '%03d' % i, 'w')

  fout.write('# Ye is constant, Ye = \n')
  fout.write(format_float(ye) + '\n')
  fout.write('# mass is constant, mass = ' + format_float(mass) + '\n')
  fout.write("# [1] time [s]\n")
  fout.write("# [2] LS220 temperature [GK]\n")
  fout.write("# [3] density [g cm^-3]\n")
  fout.write("# [4] specific energy [cm^2 s^-2]\n")
  fout.write("# [5] radius [Schwarzschild coordinates]\n")

  # loop over times
  for t in range(len(new_time)):
    fout.write(format_float(new_time[t]))
    fout.write(format_float(new_temp[t]))
    fout.write(format_float(new_rho[t]))
    fout.write(format_float(new_energy[t]))
    fout.write(format_float(new_r[t]))

    # end of line
    fout.write('\n')

  fout.close()

  sys.stdout.write('done\n')
