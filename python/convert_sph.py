#!/usr/bin/env python

import sys

# location of the *.sph files
path = '../sph_data/'

temperature_file = 'Temp'
density_file = 'Density'
ye_file = 'Ye'
energy_file = 'Energy'
coordinate_file = 'X'
entropy_file = 'Entropy'

prefix = 'Tracer'
suffix = '.sph'

# output path (will be appended _XXX where XXX is the particle number)
out_path = '../sph_particle_data/particle'

import sys
import math
import os.path

def read_sph_file(name, num_cols):
  times = []
  data = []

  # number of data points (number particles)
  # initially -1 since we don't know it beforehand
  num_data = -1

  # are we currently reading data, this is reset
  # when a blank like is encountered
  in_data = False

  # used to accumulate data
  current_data = []

  # used to skip overlapping data
  current_time = -1.0
  skip = True

  file_idx = 0
  file_name = path + '/' + prefix + name + ('%d' % file_idx) + suffix

  while (os.path.isfile(file_name)):
    print "start reading %s" % file_name
    f = open(file_name, 'r')

    line_number = -1
    for line in f:
      line = line.strip()
      line_number = line_number + 1

      if (line.startswith("#")):
        # comment line
        continue

      if (len(line) == 0):
        # empty line
        if (in_data):
          in_data = False

          if (skip):
            continue

          # if we don't already know the number of data points, set it now
          if (num_data == -1):
            num_data = len(current_data)

          # finish up accumulating data
          if (len(current_data) != num_data):
            # we got a different number of data points than before
            print("ERROR: got data set of length %i, but previously had datasets of length %i. (line # %i)" % (len(current_data), num_data, line_number))
            sys.exit(1)
          else:
            data.append(current_data)
            current_data = []
        else:
          pass # nothing to do...

      else:
        # not empty line
        if (in_data):

          if (skip):
            continue

          # we are currently reading data
          cols = line.split()

          # we might expect more columns but only get 1 with "0", which means the particle
          # fell into the black hole
          if (line.strip() == "0"):
            cols = ["0"] * num_cols

          if (len(cols) < num_cols):
            # got wrong number of columns
            print("ERROR: got data line with too few columns (%i instead of %i)." % (len(cols), num_cols))
            sys.exit(1)
          else:
            # data ok
            if (num_cols > 1):
              dat = tuple(float(x) for x in cols)
            else:
              dat = float(cols[0])
            current_data.append(dat)

        else:
          # not reading data, this must be a time
          time = float(line)

          # if this time is older than the current time, skip the data
          if (time > current_time):
            current_time = time
            times.append(time)
            skip = False
          else:
            skip = True

          # now data starts
          in_data = True

    # reached the end, save last datasets
    if (len(current_data) != num_data):
      # we got a different number of data points than before
      print("ERROR: got data set of length %i, but previously had datasets of length %i. (last line)" % (len(current_data), num_data))
      print("ignoring because it's the last line")

      # remove the last time
      del times[-1]
      #sys.exit(1)
    else:
      data.append(current_data)

    in_data = False
    skip = True
    current_data = []

    f.close()
    print "done reading %s" % file_name

    file_idx = file_idx + 1
    file_name = path + '/' + prefix + name + ('%d' % file_idx) + suffix

  return (times, data, num_data)

def check_data(num_data, times, file_times, file_data, file_num_data, file):
  if (num_data != file_num_data):
    print("ERROR: Expected %i number of particles, but found %i in %s." % (num_data, file_num_data, file))
    sys.exit(1)
  if (len(times) != len(file_data)):
    print("ERROR: Expected %i times in %s, but found %i" % (len(times), file, len(file_data)))
    sys.exit(1)
  if (times != file_times):
    print("ERROR: Found unexpected times in %s" % file)
    sys.exit(1)

def format_float(x):
  return '% 25.15E' % x

(temperature_times, temperature_data, temperature_num_data) = read_sph_file(temperature_file, 1)
(density_times, density_data, density_num_data) = read_sph_file(density_file, 1)
(ye_times, ye_data, ye_num_data) = read_sph_file(ye_file, 1)
(energy_times, energy_data, energy_num_data) = read_sph_file(energy_file, 1)
(coordinate_times, coordinate_data, coordinate_num_data) = read_sph_file(coordinate_file, 3)
(entropy_times, entropy_data, entropy_num_data) = read_sph_file(entropy_file, 1)

# verify we have the same number of particles and same times in each file
num_data = temperature_num_data
times = temperature_times

check_data(num_data, times, temperature_times, temperature_data, temperature_num_data, temperature_file)
check_data(num_data, times, density_times, density_data, density_num_data, density_file)
check_data(num_data, times, ye_times, ye_data, ye_num_data, ye_file)
check_data(num_data, times, energy_times, energy_data, energy_num_data, energy_file)
check_data(num_data, times, coordinate_times, coordinate_data, coordinate_num_data, coordinate_file)
check_data(num_data, times, entropy_times, entropy_data, entropy_num_data, entropy_file)

# get masses
f = open(path + '/' + prefix + "Density0" + suffix, 'r')

newtonian_masses = []
GR_masses = []

in_data = False
for line in f:
  line = line.strip()

  if (line.startswith("#")):
    # comment line
    continue

  if (len(line) == 0):
    # blank line, data ends
    in_data = False
    continue

  if (in_data):
    cols = line.split()
    if (len(cols) != 3):
      print("ERROR: cannot read masses, got %i columns instead of 3" % len(cols))
      sys.exit(1)

    newtonian_masses.append(float(cols[1]))
    GR_masses.append(float(cols[2]))

  else:
    # not in data, must be a time
    time = float(line)
    if (time == times[0]):
      # we are going to read the first entry
      in_data = True
    else:
      break

# check masses
if (len(newtonian_masses) != num_data):
  print("ERROR: expected %i Newtonian masses bug got %i" % (num_data, len(newtonian_masses)))
  sys.exit(1)

if (len(GR_masses) != num_data):
  print("ERROR: expected %i GR masses bug got %i" % (num_data, len(GR_masses)))
  sys.exit(1)

# now write data for each particle to a file

# loop over particles
for p in range(num_data):
  # if the coordinates (and all other quantities) are exactly 0 at the end, this particle fell into the black hole and we don't care about it
  if (coordinate_data[len(times)-1][p] == (0,0,0)):
    print("particle %i fell into black hole" % (p+1))
    continue

  # open file
  f = open(out_path + '%03d' % p, 'w')

  f.write('# Ye is constant, Ye = ' + format_float(ye_data[0][p]) + '\n')
  f.write('# Newtonian mass = ' + format_float(newtonian_masses[p]) + '\n')
  f.write('# GR mass = ' + format_float(GR_masses[p]) + '\n')
  f.write("# [1] time [s]\n")
  f.write("# [2] temperature [GK]\n")
  f.write("# [3] density [g cm^-3]\n")
  f.write("# [4] entropy [k_B / baryon]\n")
  f.write("# [5] specific kinetic + potential energy [cm^2 s^-2]\n")
  f.write("# [6] radius [Schwarzschild coordinates]\n")
  f.write("# [7] x [Schwarzschild coordinates]\n")
  f.write("# [8] y [Schwarzschild coordinates]\n")
  f.write("# [9] z [Schwarzschild coordinates]\n")
  #f.write("# [10] temperature [MeV]\n")
  #f.write("# [11] density [baryon fm^-3]\n")

  # loop over times
  for t in range(len(times)):
    # verify Ye is constant
    if (ye_data[t][p] != ye_data[0][p]):
      print("ERROR: Ye not constant, particle %i, time %i" % (p, t))
      sys.exit(1)

    # the quantities in the *.sph files use 1 = c = G = Msun, so we need to convert to cgs

    # to convert time to seconds, multiply by 1 = G Msun / c^3 = 4.9271857025367225E-06 s
    f.write(format_float(times[t] * 4.9271857025367225E-06))

    # temperature is in MeV and we want it in Giga Kelvin, so multiply by 1/kB = 11.604505289680862 GK/MeV
    f.write(format_float(temperature_data[t][p] * 11.604505289680862))

    # to convert density to g cm^-3, multiply by 1 = c^6 G^-3 Msun^-2 = 6.1754947E17 (from Matt Duez)
    f.write(format_float(density_data[t][p] * 6.1754947E17))

    # to convert entropy to k_B / baryon, multiply by 939.5
    f.write(format_float(entropy_data[t][p] * 939.5))

    # specific energy is in units of c^2, so multiply by c^2 = 8.9875517873681764E20 cm^2 s^-2
    f.write(format_float(energy_data[t][p] *8.9875517873681764E20))

    # TODO I'm not sure how to convert coordinates
    coord = coordinate_data[t][p]
    f.write(format_float(math.sqrt(coord[0]*coord[0] + coord[1]*coord[1] + coord[2]*coord[2])))
    f.write(format_float(coord[0]))
    f.write(format_float(coord[1]))
    f.write(format_float(coord[2]))

    # temperature is in MeV, which we want here
    #f.write(format_float(temperature_data[t][p]))

    # to convert density to baryon fm^-3, multiply by
    # 1 = c^6 G^-3 Msun^-2 * g cm^-3 M_neutron^-1 fm^3
    # = 6.1754947E17 (from Matt Duez) * 5.9704087045248921E-16
    #f.write(format_float(density_data[t][p][0] * 6.1754947E17 * 5.9704087045248921E-16))

    # end of line
    f.write('\n')

  f.close()
  print("written file %i of %i" % (p+1, num_data))
